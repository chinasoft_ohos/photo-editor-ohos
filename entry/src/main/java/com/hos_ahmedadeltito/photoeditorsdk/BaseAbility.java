/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk;

import com.hos_ahmedadeltito.photoeditorsdk.widget.MyDialog;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Environment;
import ohos.bundle.IBundleManager;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static com.hos_ahmedadeltito.photoeditorsdk.MainAbility.MY_PERMISSION_REQUEST_CAMERA;
import static com.hos_ahmedadeltito.photoeditorsdk.MainAbility.MY_PERMISSION_REQUEST_GALLERY;

public class BaseAbility extends AbilitySlice {
    /**
     * @param 相册回调
     */
    public final static int REQUEST_IMAGE = 201;
    /**
     * @param 相机回调
     */
    public final static int REQUEST_CODE_TAKE_PICTURE = 202;
    protected PixelMap bitmap;
    private MyDialog dialog;


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        dialog = new MyDialog(getContext());
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes();
        dialog.setSize((int) (attributes.width * 0.8), (int) (attributes.height * 0.2));
    }


    protected void startCameraActivity() {//获取权限打开相机
        if (verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED
            || verifySelfPermission("ohos.permission.READ_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED
            || verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
            dialog.setTips("To attach photos, we need to access media on your device");
            dialog.setOnclickListen(new MyDialog.OnClickListen() {
                @Override
                public void rightClick() {
                    dialog.destroy();
                    //应用权限未被授予
                    if (canRequestPermission("ohos.permission.CAMERA")
                        || canRequestPermission("ohos.permission.READ_USER_STORAGE")
                        || canRequestPermission("ohos.permission.WRITE_USER_STORAGE")) {
                        //验证是否可以申请弹窗授权（首次申请或者用户未选择禁止且不再提示）
                        requestPermissionsFromUser(new String[]{"ohos.permission.CAMERA", "ohos.permission.READ_USER_STORAGE", "ohos.permission.WRITE_USER_STORAGE"}, MY_PERMISSION_REQUEST_CAMERA);
                    } else {
                        //显示应用权限需要权限的理由，提示用户进入设置授权
                        new ToastDialog(getContext()).setText("请前往设置授予权限").setDuration(2000).show();
                    }
                }

                @Override
                public void liftClick() {
                    new ToastDialog(getContext()).setText("权限已被拒绝").setDuration(500).show();
                    dialog.destroy();
                }
            });
            dialog.showDialog();
        } else {
            openCamera();
        }
    }


    private void openCamera() {
        new ToastDialog(getContext()).setText("暂无该功能").setDuration(2000).show();
    }


    protected void startPhoto() {
        dialog.setTransparent(true);
        if (verifySelfPermission("ohos.permission.READ_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED
            || verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
            dialog.setTips("To attach photos, we need to access media on your device");
            dialog.setOnclickListen(new MyDialog.OnClickListen() {
                @Override
                public void rightClick() {
                    dialog.destroy();
                    //应用权限未被授予
                    if (canRequestPermission("ohos.permission.READ_USER_STORAGE")
                        || canRequestPermission("ohos.permission.WRITE_USER_STORAGE")) {
                        //验证是否可以申请弹窗授权（首次申请或者用户未选择禁止且不再提示）
                        requestPermissionsFromUser(new String[]{"ohos.permission.READ_USER_STORAGE", "ohos.permission.WRITE_USER_STORAGE"}, MY_PERMISSION_REQUEST_GALLERY);
                    } else {
                        //显示应用权限需要权限的理由，提示用户进入设置授权
                        new ToastDialog(getContext()).setText("请前往设置授予权限").setDuration(2000).show();
                    }
                }

                @Override
                public void liftClick() {
                    new ToastDialog(getContext()).setText("权限已被拒绝").setDuration(500).show();
                    dialog.destroy();
                }
            });
            dialog.showDialog();
        } else {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.GET_CONTENT");
            intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
            intent.setType("image/*");
            startAbilityForResult(intent, REQUEST_IMAGE);
        }
    }

    @Override
    protected void onBackground() {//销毁 用于做释放操作
        if (bitmap != null) {
            bitmap = null;
        }
        System.gc();
        super.onBackground();
    }
}
