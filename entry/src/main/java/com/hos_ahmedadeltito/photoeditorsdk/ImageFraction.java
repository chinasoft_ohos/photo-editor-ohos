/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import com.hos_ahmedadeltito.photoeditorsdk.provider.ImageAdapter;
import com.hos_ahmedadeltito.photoeditorsdk.provider.ImageClickedListener;
import com.hos_ahmedadeltito.photoeditorsdk.utils.ArrDataUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * 显示element图片
 *
 * @since 2021-04-23
 */
public class ImageFraction {
    private Context mContext;
    private ArrayList<PixelMap> stickerBitmaps;
    private ListContainer listGrid;
    private ImageClickedListener mClickedListener;

    public ImageFraction(Context context, ImageClickedListener mClickedListener) {
        this.mContext = context;
        this.mClickedListener = mClickedListener;
    }

    public Component createView() {
        Component mainComponent =
            LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_fragment_main_photo_edit_image, null, false);
        if (!(mainComponent instanceof ComponentContainer)) {
            return null;
        }
        ComponentContainer rootLayout = (ComponentContainer) mainComponent;
        initComponent(rootLayout);
        return rootLayout;
    }

    protected void initComponent(ComponentContainer container) {
        listGrid = (ListContainer) container.findComponentById(ResourceTable.Id_fragment_main_photo_edit_image_rv);
        stickerBitmaps = new ArrayList<>();
        int[] images = ArrDataUtil.getImages();
        for (int i = 0; i < images.length; i++) {
            stickerBitmaps.add(decodeSampledBitmapFromResource(images[i], 120, 120));
        }
        ImageAdapter imageAdapter = new ImageAdapter(mContext, stickerBitmaps);
        imageAdapter.setClickedListener(mClickedListener);
        listGrid.setItemProvider(imageAdapter);

    }

    public PixelMap decodeSampledBitmapFromResource(int resId, int reqWidth, int reqHeight) {
        ImageSource imageSource = null;
        ImageSource.DecodingOptions options = null;
        try (InputStream inputStream = mContext.getResourceManager().getResource(resId)) {
            imageSource = ImageSource.create(inputStream, null);
            options = new ImageSource.DecodingOptions();
            options.desiredSize = new Size(reqWidth, reqHeight);
            options.desiredRegion = new Rect(0, 0, 0, 0);

        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageSource.createPixelmap(options);
    }
}
