/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk.widget;

import ohos.media.image.Image;
import ohos.media.image.common.ImageFormat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Csj
 * @date 2021/3/11
 */
public class ImageSaver implements Runnable {
    private final static String TAG = "ImageSaver";

    private Image image;
    private File file;
    private ImageSaverCallback imageSaverCallback;

    public interface ImageSaverCallback {
        void onSuccessFinish(byte[] bytes);

        void onError();
    }

    public ImageSaver(Image image, File file) {
        this.image = image;
        this.file = file;
    }

    public ImageSaver(Image image, File file, ImageSaverCallback imageSaverCallback) {
        this.image = image;
        this.file = file;
        this.imageSaverCallback = imageSaverCallback;
    }

    @Override
    public void run() {
        FileOutputStream output = null;
        try {
            Image.Component component = image.getComponent(ImageFormat.ComponentType.JPEG);
            byte[] bytes = new byte[component.remaining()];
            component.read(bytes);
            output = new FileOutputStream(file);
            output.write(bytes); // 写图像数据
            if (imageSaverCallback != null) {
                imageSaverCallback.onSuccessFinish(bytes);
            }
        } catch (IOException e) {
//            Log.e(TAG, "Can't save the image file.");
            if (imageSaverCallback != null) {
                imageSaverCallback.onError();
            }
        } catch (IllegalStateException e) {
//            Log.e(TAG, "Can't read the image file.", e);
            if (imageSaverCallback != null) {
                imageSaverCallback.onError();
            }
        } finally {
            if (image != null) {
                image.release();
            }
            if (output != null) {
                try {
                    output.close(); // 关闭流
                } catch (IOException e) {
//                    Log.e(TAG, "image release occur exception!");
                }
            }
        }
    }

}
