/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk.widget;

import com.hos_ahmedadeltito.photoeditorsdk.ResourceTable;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.global.resource.Resource;


import java.lang.ref.WeakReference;

/**
 * 自定义的弹框用于权限获取提示
 *
 * @since 2021-04-06
 */
public class MyDialog extends CommonDialog {

    WeakReference<Context> context;
    int defaultLayout = ResourceTable.Layout_dialog;
    int layout = -1;
    String tips = "Hello World!!!";
    private OnClickListen onclickListen;
    private SetComponent setComponent;

    public MyDialog(Context context) {
        super(context);
        this.context = new WeakReference<Context>(context);
        //this.setTransparent(true);

    }

    public int getDefaultLayout() {
        return defaultLayout;
    }

    public void setTips(String tip) {
        this.tips = tip;
    }


    public void init() {
        Component cpt;
        LayoutScatter layoutScatter = LayoutScatter.getInstance(context.get());
        if (layout > -1) {
            cpt = layoutScatter.parse(layout, null, false);
            setComponent.setComponent(cpt);
        } else {
            cpt = layoutScatter.parse(defaultLayout, null, false);
            ((Text) cpt.findComponentById(ResourceTable.Id_tv_tips)).setText(tips);
            ((Button) cpt.findComponentById(ResourceTable.Id_bt_left)).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (onclickListen != null) {
                        onclickListen.liftClick();
                    }
                }
            });

            ((Button) cpt.findComponentById(ResourceTable.Id_bt_right)).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (onclickListen != null) {
                        onclickListen.rightClick();
                    }
                }
            });
        }

        setContentCustomComponent(cpt);
        setTransparent(true);
    }

    public void showDialog() {
        init();
        show();
    }

    public void setOnclickListen(OnClickListen onclickListen) {
        this.onclickListen = onclickListen;
    }

    public interface OnClickListen {
        void rightClick();

        void liftClick();
    }

    public void setComponent(SetComponent setComponent) {
        if (layout > -1) {
            this.setComponent = setComponent;
        } else {
            throw new NullPointerException(" No settings layout");
        }
    }

    public interface SetComponent {
        void setComponent(Component cpt);
    }

}
