/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk.provider;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import com.hos_ahmedadeltito.photoeditorsdk.BaseFraction;

import java.util.ArrayList;
import java.util.List;

/**
 * PageSlider页面适配器
 *
 * @since 2021-04-25
 */
public class PagerProvider extends PageSliderProvider {
    private List<Component> pages = new ArrayList<>();

    /**
     * MainPagerAdapter
     *
     * @param pages 列表数据
     */
    public PagerProvider(List<Component> pages) {
        this.pages.addAll(pages);
    }

    @Override
    public int getCount() {
        return pages == null ? 0 : pages.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        componentContainer.addComponent(pages.get(i));
        return componentContainer;

    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent(pages.get(i));
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }
}
