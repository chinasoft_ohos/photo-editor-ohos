/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk.provider;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import com.hos_ahmedadeltito.photoeditorsdk.LogUtils;
import com.hos_ahmedadeltito.photoeditorsdk.ResourceTable;
import com.hos_ahmedadeltito.photoeditorsdk.utils.DeviceUtil;
import com.hos_ahmedadeltito.photoeditorsdk.utils.FontUtil;

import java.util.List;

/**
 * @since 2021-04-22
 */
public class EmojiAdapter extends BaseItemProvider {

    private Context mContext;
    private List<String> list;
    private Font emojiFont;
    private final int LINE_COUNT = 4;
    private int mItemWidth;
    private EmojiClickedListener mEmojiClickedListener;

    public void setEmojiClickedListener(EmojiClickedListener mEmojiClickedListener) {
        this.mEmojiClickedListener = mEmojiClickedListener;
    }

    public EmojiAdapter(Context context, List list,Font emojiFont) {
        this.mContext = context;
        this.list = list;
        this.emojiFont = emojiFont;
        mItemWidth = DeviceUtil.getScreenWidth(mContext) / LINE_COUNT;
    }

    @Override
    public int getCount() {
        int needLines = list.size() / LINE_COUNT;
        if (list.size() % LINE_COUNT > 0)
            needLines++;
        return needLines;
    }

    @Override
    public Object getItem(int i) {
        int startIndex = i * LINE_COUNT;
        int endIndex = (i + 1) * LINE_COUNT;
        if (endIndex > list.size())
            endIndex = list.size();
        //此处返回一行的数据列表
        return list.subList(startIndex, endIndex);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component == null) {
            component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_provider_test_grid_item,
                null, false);
        }
        //移除所有子控件
        ((DirectionalLayout)component).removeAllComponents();
        //获取一行的数据
        List<String> lineItems = (List<String>) getItem(i);
        //添加每一行的控件
        for (String emojiId : lineItems) {


            Text text = new Text(mContext);
//            text.setPadding(0, 40, 0, 40);
//            text.setMarginsTopAndBottom(0, 0);
            text.setTextSize(35, Text.TextSizeType.FP);
            text.setTextAlignment(TextAlignment.CENTER);
            text.setText(FontUtil.convertEmoji(emojiId));
            text.setFont(emojiFont);
            DirectionalLayout.LayoutConfig itemLayoutConfig = new DirectionalLayout.LayoutConfig(mItemWidth, mItemWidth);

            if (mEmojiClickedListener != null) {
                text.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        mEmojiClickedListener.onClick(text,text.getText(),text.getFont());
                    }
                });
            }
            ((DirectionalLayout)component).addComponent(text, itemLayoutConfig);
        }
        return component;
    }
}
