/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk.utils;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * 获取设备信息
 *
 * @since 2021-04-22
 */
public class DeviceUtil {

    /**
     * 获取屏幕宽
     *
     * @param context
     * @return 屏幕宽
     */
    public static int getScreenWidth(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return displayAttributes.width;
    }

    /**
     * 获取屏幕高
     *
     * @param context
     * @return 屏幕高
     */
    public static int getScreenHeight(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return displayAttributes.height;
    }

    /**
     * 获取屏幕X轴像素密度
     *
     * @param context
     * @return X轴像素密度
     */
    public static float getWidthDpi(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return displayAttributes.xDpi;
    }

    /**
     * 获取屏幕Y轴像素密度
     *
     * @param context
     * @return Y轴像素密度
     */
    public static float getHeightDpi(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return displayAttributes.yDpi;
    }
}
