/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk.utils;

import com.hos_ahmedadeltito.photoeditorsdk.ResourceTable;

/**
 * 存放数组
 *
 * @since 2021-04-25
 */
public class ArrDataUtil {
    public static int[] getImages() {
        return new int[]{
            ResourceTable.Media_c,
            ResourceTable.Media_d,
            ResourceTable.Media_e,
            ResourceTable.Media_i,
            ResourceTable.Media_j,
            ResourceTable.Media_k,
            ResourceTable.Media_l,
            ResourceTable.Media_m,
            ResourceTable.Media_n,
            ResourceTable.Media_o,
            ResourceTable.Media_p,
            ResourceTable.Media_q,
            ResourceTable.Media_r,
            ResourceTable.Media_s,
            ResourceTable.Media_t,
            ResourceTable.Media_u,
            ResourceTable.Media_v,
            ResourceTable.Media_w,
            ResourceTable.Media_x,
            ResourceTable.Media_y,
            ResourceTable.Media_z,
            ResourceTable.Media_aa,
            ResourceTable.Media_bb,
            ResourceTable.Media_cc,
            ResourceTable.Media_dd,
            ResourceTable.Media_ee
        };
    }
}
