/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk.slice;

import com.hos_ahmedadeltito.photoeditorsdk.BaseAbility;
import com.hos_ahmedadeltito.photoeditorsdk.LogUtils;
import com.hos_ahmedadeltito.photoeditorsdk.ResourceTable;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;


public class MainAbilitySlice extends BaseAbility implements Component.ClickedListener {

    private String uriString;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Button btn_photoalbum = (Button) findComponentById(ResourceTable.Id_btn_photoalbum);
        Button camera = (Button) findComponentById(ResourceTable.Id_btn_camera);
        camera.setClickedListener(this);
        btn_photoalbum.setClickedListener(this);

    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (requestCode == REQUEST_IMAGE) {
            if (resultData != null && null != resultData.getUri() && !resultData.getUriString().isEmpty()) {
                uriString = resultData.getUriString();
            }
        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            if (resultData != null && resultData.getUri() != null && !resultData.getUriString().isEmpty()) {
                uriString = resultData.getUriString();
            }
        }
        if (uriString != null && !uriString.isEmpty()) {
            LogUtils.println("图片Uri = " + uriString);
            photoShow();
        }
    }

    private void photoShow() {
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
            .withDeviceId("")
            .withBundleName("com.hos_ahmedadeltito.photoeditorsdk")
            .withAbilityName("com.hos_ahmedadeltito.photoeditorsdk.PhotoEditorAbility")
            .build();
        intent1.setParam("key", uriString);
        // 把operation设置到intent中
        intent1.setOperation(operation);
        startAbility(intent1);
    }

    private void openUserCamera(Component view) {
        startCameraActivity();
    }

    private void onPhotoTaken() {
        startPhoto();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_photoalbum:
                onPhotoTaken();
                break;
            case ResourceTable.Id_btn_camera:
                openUserCamera(component);
                break;
            default:
                break;
        }
    }


}
