/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import com.hos_ahmedadeltito.photoeditorsdk.provider.EmojiAdapter;
import com.hos_ahmedadeltito.photoeditorsdk.provider.EmojiClickedListener;
import com.hos_ahmedadeltito.photoeditorsdk.utils.JavaBlurProcess;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 显示emoji表情
 *
 * @since 2021-04-23
 */
public class EmojiFraction {

    private Context mContext;
    private ArrayList<String> emojiIds;
    private EmojiClickedListener mEmojiClickedListener;
    private Font emojiFont;


    public EmojiFraction(Context context, Font emojiFont, EmojiClickedListener mEmojiClickedListener) {
        this.mContext = context;
        this.mEmojiClickedListener = mEmojiClickedListener;
        this.emojiFont = emojiFont;

    }

    public Component createView() {
        Component mainComponent =
            LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_fragment_main_photo_edit_image, null, false);
        if (!(mainComponent instanceof ComponentContainer)) {
            return null;
        }

        Image gostImage = (Image) mainComponent.findComponentById(ResourceTable.Id_gost_image);
       /* Texture texture = new Texture(gostImage.getWidth(),gostImage.getHeight(),Texture.ColorType.BGRA_8888_COLORTYPE,Texture.AlphaType.OPAQUE_ALPHATYPE);
        Canvas canvas = new Canvas(texture);
        int color = Color.getIntColor("#AAFFFFFF");
        canvas.drawColor(color,Canvas.PorterDuffMode.DARKEN);*/


//        try {
//            ShapeElement shapeElement = new ShapeElement(mContext, ResourceTable.Graphic_dilog_bg);
//            shapeElement.setRgbColor(RgbColor.fromArgbInt(mContext.getResourceManager().getElement(ResourceTable.Color_black).getColor()));
//            shapeElement.setAlpha(150);
//            gostImage.setBackground(shapeElement);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (NotExistException e) {
//            e.printStackTrace();
//        } catch (WrongTypeException e) {
//            e.printStackTrace();
//        }


        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(500, 800);
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        PixelMap pixelMap = PixelMap.create(options);

        Canvas canvas = new Canvas(new Texture(pixelMap));
        Paint paint = new Paint();
        //paint.setAlpha(0.9f);

        paint.setColor(new Color(Color.getIntColor("#ff000000")));
        PixelMapHolder p = new PixelMapHolder(pixelMap);
        canvas.drawPixelMapHolder(p, 0, 0, paint);
        pixelMap = p.getPixelMap();


        JavaBlurProcess blurProcess = new JavaBlurProcess();
        blurProcess.blur(pixelMap, 10);
        gostImage.setPixelMap(pixelMap);
//        gostImage

        ComponentContainer rootLayout = (ComponentContainer) mainComponent;
        initComponent(rootLayout);
        return rootLayout;
    }


    protected void initComponent(ComponentContainer root) {
        String[] emojis = mContext.getStringArray(ResourceTable.Strarray_photo_editor_emoji);
        emojiIds = new ArrayList<>();
        Collections.addAll(emojiIds, emojis);


        ListContainer listGrid = (ListContainer) root.findComponentById(ResourceTable.Id_fragment_main_photo_edit_image_rv);
        EmojiAdapter emojiAdapter = new EmojiAdapter(mContext, emojiIds, emojiFont);
        emojiAdapter.setEmojiClickedListener(mEmojiClickedListener);
        listGrid.setItemProvider(emojiAdapter);
    }
}
