/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk;

import com.hos_ahmedadeltito.photoeditorsdk.slice.MainAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.bundle.IBundleManager;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;


public class MainAbility extends Ability {
    public static int MY_PERMISSION_REQUEST_CAMERA = 80;
    public static int MY_PERMISSION_REQUEST_GALLERY = 90;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        WindowManager.getInstance().getTopWindow().get().setStatusBarVisibility(Component.HIDE);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#3F51B5"));
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSION_REQUEST_GALLERY){
            if (verifySelfPermission("ohos.permission.READ_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED
                || verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
                new ToastDialog(getContext()).setText("权限已被拒绝").setDuration(500).show();
            }else {
                new ToastDialog(getContext()).setText("权限已被授予").setDuration(500).show();
            }
        }else if (requestCode == MY_PERMISSION_REQUEST_CAMERA){
            if (verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED
                || verifySelfPermission("ohos.permission.READ_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED
                || verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
                new ToastDialog(getContext()).setText("权限已被拒绝").setDuration(500).show();
            }else {
                new ToastDialog(getContext()).setText("权限已被授予").setDuration(500).show();
            }
        }
    }
}
