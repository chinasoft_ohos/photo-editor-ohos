/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

public class ColorPickerAdapter extends BaseItemProvider {
    private List<ColorBean> list;
    private AbilitySlice slice;
    private Context mContext;

    public ColorPickerAdapter(Context mContext, List<ColorBean> list, AbilitySlice slice) {
        this.mContext = mContext;
        this.list = list;
        this.slice = slice;

    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        if (list != null && i >= 0 && i < list.size()) {
            return list.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        if (component == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_color_picker_item_list, null, false);
        } else {
            cpt = component;
        }
        ColorBean sampleItem = list.get(i);
        Image text = (Image) cpt.findComponentById(ResourceTable.Id_color_picker_view);

        ShapeElement shapeElement = new ShapeElement(mContext, ResourceTable.Graphic_item_color_seleter);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(sampleItem.getColor())));
        text.setBackground(shapeElement);
        return cpt;
    }


}
