# photo-editor-ohos
#### 项目介绍

- 项目名称：photo-editor-ohos
- 所属系列：openharmony 第三方组件适配移植
- 功能：它是一个ohos图像编辑SDK，简单，易于支持图像操作
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 1.0

#### 效果演示

![photo-editor-ohos](/screenshots/photo-editor-ohos.gif)

#### 安装教程

1.在项目根目录下的build.gradle文件中，

```JAVA
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```JAVA
dependencies {
   implementation('com.gitee.chinasoft_ohos:photo-editor-ohos:1.0.1')
   ......  
}
```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


-----------------------------------------------------------------------------------------------------

#### 使用说明

1. 首先，您必须获取PhotoEditorSDK的实例来初始化它并开始调用所需的函数.
```java
photoEditorSDK = new PhotoEditorSDK.PhotoEditorSDKBuilder(PhotoEditorActivity.this)
.parentView(parentImageRelativeLayout)
//add parent image view
.childView(photoEditImageView)
//add the desired image view
.deleteView(deleteRelativeLayout)
//add the deleted view that will appear during the movement of the views
.brushDrawingView(brushDrawingView)
// add the brush drawing view that is responsible for drawing on the image view
.buildPhotoEditorSDK();
// build photo editor sdk
```

2. 在图像上添加文本 **文本**:
```java
photoEditorSDK.addText(text, colorCodeTextView);
```

3. 添加 **图像** 或 **贴纸**:
```java
photoEditorSDK.addImage(image);
```

4. 添加 **表情符号**:
```java
photoEditorSDK.addEmoji(emojiName, emojiFont);
```

5. 在图像上 **画**:
```java
photoEditorSDK.setBrushDrawingMode(brushDrawingMode);
// brushDrawingMode is false by default, true if you want to draw on the image view
```

6. 更改工程视图的 **颜色** 和 **大小** 以及 **橡皮擦** 视图的 **大小** 和 **颜色**:
```java
photoEditorSDK.setBrushSize(size);
photoEditorSDK.setBrushColor(colorCode);
photoEditorSDK.brushEraser();
photoEditorSDK.setBrushEraserSize(brushEraserSize);
photoEditorSDK.setBrushEraserColor(color);
```

7. **保存** 编辑后的图像:
```java
photoEditorSDK.saveImage(folderName, imageName);
```

8. To **Undo** the added **Views (Image or Text)**:
```java
photoEditorSDK.viewUndo();
```

9. 要 **清除所有** 添加的 **视图 (图像或文本)**:
```java
photoEditorSDK.clearAllViews();
```

10. 要 **清除所有** 添加的 **工程视图**:
```java
photoEditorSDK.clearBrushAllViews();
```

11. 要收听 **添加视图**, **编辑添加的文本视图**, **添加和删除的视图** and **Start and Stop Moving Views**. You can implement:
```java
photoEditorSDK.setOnPhotoEditorSDKListener(new OnPhotoEditorSDKListener() {
    @Override
	public void onEditTextChangeListener(String text, int colorCode) {

	}
	@Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {

    }
    @Override
	public void onRemoveViewListener(int numberOfAddedViews) {

	}
    @Override
	public void onStartViewChangeListener(ViewType viewType) {

	}
    @Override
	public void onStopViewChangeListener(ViewType viewType) {
  
	}
});
```
#### 测试信息
CodeCheck代码测试无异常  
CloudTest代码测试无异常  
安全病毒安全检测通过  
当前版本demo功能与原组件基本无差异  

#### 版本迭代
- 1.0.1 

#### 版权和许可信息  
- Copyright (c) 2017 Eventtus, PhotoEditorSDK is released under the MIT license.
