/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos_ahmedadeltito.photoeditorsdk;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.logging.Logger;

/**
 * 日志打印统一处理
 *
 * @since 2021-04-23
 */
public class LogUtils {
    private static final HiLogLabel BRUSH_DRAWING = new HiLogLabel(HiLog.LOG_APP, 0x00201, "相机SDK");

    public static void showLog(Object string) {
        HiLog.info(BRUSH_DRAWING, "日志打印 : " + string);
//        Logger.getGlobal().warning("日志打印 : "+string);
    }

    public static void println(Object string) {
        System.out.println("日志打印 : " + string);
    }
}
